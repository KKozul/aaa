﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Korisnik
    {

        public int IDKorisnik { get; set; }
        public string ImeKorisnik { get; set; }
        public string PrezimeKorisnik { get; set; }
        public DateTime DOBKorisnik { get; set; }
        public string UsernameKorisnik { get; set; }
        public string EmailKorisnik { get; set; }
        public int VisinaKorisnik { get; set; }
        public int TezinaKorisnik { get; set; }
        public int RazinaFizickeAktivnostiKorisnik { get; set; }
        public int TipDijabetesaKorisnik { get; set; }
        public string SpolKorisnik { get; set; }

        public Korisnik(int IDKorisnik, string ImeKorisnik, string PrezimeKorisnik, DateTime DOBKorisnik, string UsernameKorisnik, string EmailKorisnik, int VisinaKorisnik, int TezinaKorisnik, int RazinaFizickeAktivnostiKorisnik, int TipDijabetesaKorisnik, string SpolKorisnik)
        {
            this.IDKorisnik = IDKorisnik;
            this.ImeKorisnik = ImeKorisnik;
            this.PrezimeKorisnik = PrezimeKorisnik; 
            this.DOBKorisnik = DOBKorisnik; 
            this.UsernameKorisnik = UsernameKorisnik; 
            this.EmailKorisnik = EmailKorisnik; 
            this.VisinaKorisnik = VisinaKorisnik; 
            this.TezinaKorisnik = TezinaKorisnik; 
            this.RazinaFizickeAktivnostiKorisnik = RazinaFizickeAktivnostiKorisnik; 
            this.TipDijabetesaKorisnik = TipDijabetesaKorisnik;
            this.SpolKorisnik = SpolKorisnik;
        }
    }
}
