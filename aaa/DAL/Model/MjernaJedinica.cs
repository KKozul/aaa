﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class MjernaJedinica
    {
        public int IDMjernaJedinica { get; set; }
        public string NazivMjernaJedinica { get; set; }
        public bool OmogucenaMjernaJedinica { get; set; }

        public MjernaJedinica(int IDMjernaJedinica, string NazivMjernaJedinica, bool OmogucenaMjernaJedinica)
        {
            this.IDMjernaJedinica = IDMjernaJedinica;
            this.NazivMjernaJedinica = NazivMjernaJedinica;
            this.OmogucenaMjernaJedinica = OmogucenaMjernaJedinica;
        }
    }
}
