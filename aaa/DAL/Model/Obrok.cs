﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Obrok
    {
        public int IDObrok { get; set; }
        public string NazivObrok { get; set; }
        public bool OmogucenObrok { get; set; }

        public Obrok(int IDObrok, string NazivObrok, bool OmogucenObrok)
        {
            this.IDObrok = IDObrok;
            this.NazivObrok = NazivObrok;
            this.OmogucenObrok = OmogucenObrok;
        }
    }
}
