use dbjavaexam
go
 
go
Create table BasicDetails(
IDBasicDetails int primary key identity,
FullName nvarchar(100),
 Sex nvarchar(1),
 DateOfBirth nvarchar(100),
 )
 go
CREATE PROCEDURE CreateBasicDetails


@FullName nvarchar(100),
 @Sex nvarchar(1),
 @DateOfBirth nvarchar(100),
	@IDBasicDetails INT OUTPUT
AS 
BEGIN 
	INSERT INTO BasicDetails VALUES(@FullName, @Sex, @DateOfBirth)
	SET @IDBasicDetails = SCOPE_IDENTITY()
END
GO
CREATE PROCEDURE getBasicDetails
	@IDBasicDetails INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		BasicDetails
	WHERE 
		IDBasicDetails = @IDBasicDetails
END
GO



CREATE PROCEDURE updateBasicDetails

@FullName nvarchar(100),
 @Sex nvarchar(1),
 @DateOfBirth nvarchar(100),
	@IDBasicDetails INT
	 
AS 
BEGIN 
	UPDATE BasicDetails SET 
		FullName = @FullName, 
		Sex = @Sex, 
		DateOfBirth = @DateOfBirth
	WHERE 
		IDBasicDetails = @IDBasicDetails
END
GO


CREATE PROCEDURE deleteBasicDetails
	@IDBasicDetails INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			BasicDetails
	WHERE 
		IDBasicDetails = @IDBasicDetails

END
GO

 Create Table PersonalDetails(
 IDPersonalDetails int primary key identity,
 MaritalStatus nvarchar(20),
 NoOfDependents nvarchar(20),
 PHeight nvarchar(20),
 PWeight nvarchar(20),
 BloodTypeRH nvarchar(9)
 )
 go
CREATE PROCEDURE CreatePersonalDetails
 @MaritalStatus nvarchar(20),
 @NoOfDependents nvarchar(20),
 @PHeight nvarchar(20),
 @PWeight nvarchar(20),
 @BloodTypeRH nvarchar(9),
	@IDPersonalDetails INT OUTPUT
AS 
BEGIN 
	INSERT INTO PersonalDetails VALUES(@MaritalStatus, @NoOfDependents, @PHeight,@PWeight,@BloodTypeRH)
	SET @IDPersonalDetails = SCOPE_IDENTITY()
END
GO
CREATE PROCEDURE getPersonalDetails
	@IDPersonalDetails INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		PersonalDetails
	WHERE 
		IDPersonalDetails = @IDPersonalDetails
END
GO


CREATE PROCEDURE updatePersonalDetails

 @MaritalStatus nvarchar(20),
 @NoOfDependents nvarchar(20),
 @PHeight nvarchar(20),
 @PWeight nvarchar(20),
 @BloodTypeRH nvarchar(9),
	@IDPersonalDetails INT
	 
AS 
BEGIN 
	UPDATE PersonalDetails SET 
		MaritalStatus = @MaritalStatus, 
		NoOfDependents = @NoOfDependents, 
		PHeight = @PHeight,
				PWeight = @PWeight, 
		BloodTypeRH = @BloodTypeRH
		
	WHERE 
		IDPersonalDetails = @IDPersonalDetails
END
GO


CREATE PROCEDURE deletePersonalDetails
	@IDPersonalDetails INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			PersonalDetails
	WHERE 
		IDPersonalDetails = @IDPersonalDetails

END
GO

Create Table Adress(
IDAddres int primary key identity,
Pincode nvarchar(40),
StateName nvarchar(40),
City nvarchar(40),
Area nvarchar(40),
Street nvarchar(40),
DoorNo nvarchar(40),
)
go
CREATE PROCEDURE CreateAdress

@Pincode nvarchar(40),
@StateName nvarchar(40),
@City nvarchar(40),
@Area nvarchar(40),
@Street nvarchar(40),
@DoorNo nvarchar(40),
	@IDAddres INT OUTPUT
AS 
BEGIN 
	INSERT INTO Adress VALUES(@Pincode, @StateName, @City,@Area,@Street,@DoorNo)
	SET @IDAddres = SCOPE_IDENTITY()
END
GO
CREATE PROCEDURE getAdress
	@IDAddres INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Adress
	WHERE 
		IDAddres = @IDAddres
END
GO

CREATE PROCEDURE updateAdress

@Pincode nvarchar(40),
@StateName nvarchar(40),
@City nvarchar(40),
@Area nvarchar(40),
@Street nvarchar(40),
@DoorNo nvarchar(40),
	@IDAddres INT 
	 
AS 
BEGIN 
	UPDATE Adress SET 
		
		Pincode = @Pincode, 
		StateName = @StateName, 
		City = @City,
				Area = @Area, 
		Street = @Street,
		DoorNo=@DoorNo
	WHERE 
		IDAddres = @IDAddres
END
GO

CREATE PROCEDURE deleteAdress
	@IDAdress INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Adress
	WHERE 
		IDAddres = @IDAdress

END
GO

Create Table PhoneContacts(
IDContact int primary key identity,
 TelephoneWork nvarchar(20),
 TelephoneHome nvarchar(20),
 TelephoneMobile nvarchar(20),
 TelephonePager nvarchar(20),
 TelephoneFax nvarchar(20),
 Email nvarchar(max)
)
go

CREATE PROCEDURE CreatePhoneContacts

 @TelephoneWork nvarchar(20),
 @TelephoneHome nvarchar(20),
 @TelephoneMobile nvarchar(20),
 @TelephonePager nvarchar(20),
 @TelephoneFax nvarchar(20),
 @Email nvarchar(max),
	@IDContact INT OUTPUT
AS 
BEGIN 
	INSERT INTO PhoneContacts VALUES(@TelephoneWork, @TelephoneHome, @TelephoneMobile,@TelephonePager,@TelephoneFax,@Email)
	SET @IDContact = SCOPE_IDENTITY()
END
GO
CREATE PROCEDURE getPhoneContacts
	@IDContact INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		PhoneContacts
	WHERE 
		IDContact = @IDContact
END
GO

CREATE PROCEDURE updatePhoneContacts

 @TelephoneWork nvarchar(20),
 @TelephoneHome nvarchar(20),
 @TelephoneMobile nvarchar(20),
 @TelephonePager nvarchar(20),
 @TelephoneFax nvarchar(20),
 @Email nvarchar(max),
	@IDContact INT
	 
AS 
BEGIN 
	UPDATE PhoneContacts SET 
		
		TelephoneWork = @TelephoneWork, 
		TelephoneHome = @TelephoneHome, 
		TelephoneMobile = @TelephoneMobile,
				TelephonePager = @TelephonePager, 
		TelephoneFax = @TelephoneFax,
		Email=@Email
	WHERE 
		IDContact = @IDContact
END
GO


CREATE PROCEDURE deletePhoneContacts
	@IDContact INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			PhoneContacts
	WHERE 
		IDContact = @IDContact

END
GO

 Create Table ContactDetails(
 IDContactDetails int primary key identity,
PresentAddress  int foreign key references Adress(IDAddres),
PermanentAddress int foreign key references Adress(IDAddres),
  PhoneContact int foreign key references PhoneContacts(IDContact)

)
go

create PROCEDURE CreateContactDetails

@PresentAddress  int,
@PermanentAddress int,
  @PhoneContact int,

	@IDContactDetails INT OUTPUT
AS 
BEGIN 
	INSERT INTO ContactDetails VALUES(@PresentAddress, @PermanentAddress,@PhoneContact)
	SET @IDContactDetails = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getContactDetails
	@IDContactDetails INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		ContactDetails
	WHERE 
		IDContactDetails = @IDContactDetails
END
GO

CREATE PROCEDURE updateContactDetails


@PresentAddress  int,
@PermanentAddress int,
  @PhoneContact int,

	@IDContactDetails INT
	 
AS 
BEGIN 
	UPDATE ContactDetails SET 
		
		PresentAddress = @PresentAddress, 
		PermanentAddress = @PermanentAddress, 
		PhoneContact = @PhoneContact
			
	WHERE 
		IDContactDetails = @IDContactDetails
END
GO

CREATE PROCEDURE deleteContactDetails
	@IDContactDetails INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			ContactDetails
	WHERE 
		IDContactDetails = @IDContactDetails

END
GO

Create Table ContactNextOfKin(
IDNextOfKin int primary key identity,
 NOKNAme nvarchar(20),
 RelationshipToPatient nvarchar(20),
  BasicDetails int foreign key references BasicDetails(IDBasicDetails),
  ContactDetail int foreign key references ContactDetails(IDContactDetails)
)
go

CREATE PROCEDURE CreateContactNextOfKin

 @NOKNAme nvarchar(20),
 @RelationshipToPatient nvarchar(20),
  @BasicDetails int ,
  @ContactDetail int,
	@IDNextOfKin INT OUTPUT
AS 
BEGIN 
	INSERT INTO ContactNextOfKin VALUES(@NOKNAme, @RelationshipToPatient, @BasicDetails, @ContactDetail)
	SET @IDNextOfKin = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getContactNextOfKin
	@IDNextOfKin INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		ContactNextOfKin
	WHERE 
		IDNextOfKin = @IDNextOfKin
END
GO

CREATE PROCEDURE updateContactNextOfKin
@NOKNAme nvarchar(20),
 @RelationshipToPatient nvarchar(20),
  @BasicDetails int ,
  @ContactDetail int,
	@IDNextOfKin INT 
	 
AS 
BEGIN 
	UPDATE ContactNextOfKin SET 
		
		NOKNAme = @NOKNAme, 
		RelationshipToPatient = @RelationshipToPatient, 
		BasicDetails = @BasicDetails,
		ContactDetail=@ContactDetail
			
	WHERE 
		IDNextOfKin = @IDNextOfKin
END
GO

CREATE PROCEDURE deleteContactNextOfKin
	@IDNextOfKin INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			ContactNextOfKin
	WHERE 
		IDNextOfKin = @IDNextOfKin

END
GO

  Create Table ProfessionDetails(
  IDProfessionDetails int primary key identity,
 Occupation nvarchar(20),
 GrossAnualIncome nvarchar(20),
)
go

CREATE PROCEDURE CreateProfessionDetails

 @Occupation nvarchar(20),
 @GrossAnualIncome nvarchar(20),
	@IDProfessionDetails INT OUTPUT
AS 
BEGIN 
	INSERT INTO ProfessionDetails VALUES(@Occupation, @GrossAnualIncome)
	SET @IDProfessionDetails = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getProfessionDetails
	@IDProfessionDetails INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		ProfessionDetails
	WHERE 
		IDProfessionDetails = @IDProfessionDetails
END
GO

CREATE PROCEDURE updateProfessionDetails

 @Occupation nvarchar(20),
 @GrossAnualIncome nvarchar(20),
	@IDProfessionDetails INT 
	 
AS 
BEGIN 
	UPDATE ProfessionDetails SET 
		
		Occupation = @Occupation, 
		GrossAnualIncome = @GrossAnualIncome
	WHERE 
		IDProfessionDetails = @IDProfessionDetails
END
GO

CREATE PROCEDURE deleteProfessionDetails
	@IDProfessionDetails INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			ProfessionDetails
	WHERE 
		IDProfessionDetails = @IDProfessionDetails

END
GO

create Table Lifestyle(
IDLifestyle int primary key identity,
 VegetarianNonVegetarian bit,
 Smoker  bit,
  ConsumeAlcoholicBeverage   bit,

  UseStimulants nvarchar(max),
 ConsumptionofCoffeeTeaDay int,
  ConsumptionofSoftDrinksDay int,
   AverageNoofCigarettesDay int,
  AverageNoofDrinksDay int,

  HaveRegularMeals nvarchar(max),
 EatHomeFoodOutsidePredominantly nvarchar(max),
)
go

CREATE PROCEDURE CreateLifestyle

 @VegetarianNonVegetarian bit,
 @Smoker  bit,
  @ConsumeAlcoholicBeverage   bit,

  @UseStimulants nvarchar(max),
 @ConsumptionofCoffeeTeaDay int,
  @ConsumptionofSoftDrinksDay int,
   @AverageNoofCigarettesDay int,
  @AverageNoofDrinksDay int,

  @HaveRegularMeals nvarchar(max),
 @EatHomeFoodOutsidePredominantly nvarchar(max),
	@IDLifestyle INT OUTPUT
AS 
BEGIN 
	INSERT INTO Lifestyle VALUES(@VegetarianNonVegetarian, @Smoker,
	 @ConsumeAlcoholicBeverage,@UseStimulants,@ConsumptionofCoffeeTeaDay,@ConsumptionofSoftDrinksDay, @AverageNoofCigarettesDay
	 ,@AverageNoofDrinksDay,@HaveRegularMeals,@EatHomeFoodOutsidePredominantly)
	SET @IDLifestyle = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getLifestyle
	@IDLifestyle INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Lifestyle
	WHERE 
		IDLifestyle = @IDLifestyle
END
GO

CREATE PROCEDURE updateLifestyle

@VegetarianNonVegetarian bit,
 @Smoker  bit,
  @ConsumeAlcoholicBeverage   bit,

  @UseStimulants nvarchar(max),
 @ConsumptionofCoffeeTeaDay int,
  @ConsumptionofSoftDrinksDay int,
   @AverageNoofCigarettesDay int,
  @AverageNoofDrinksDay int,

  @HaveRegularMeals nvarchar(max),
 @EatHomeFoodOutsidePredominantly nvarchar(max),
	@IDLifestyle INT 
	 
AS 
BEGIN 
	UPDATE Lifestyle SET 
		
		UseStimulants = @UseStimulants, 
		VegetarianNonVegetarian = @VegetarianNonVegetarian,
		Smoker = @Smoker,
		ConsumeAlcoholicBeverage = @ConsumeAlcoholicBeverage,
		AverageNoofCigarettesDay = @AverageNoofCigarettesDay,
		AverageNoofDrinksDay = @AverageNoofDrinksDay,
		ConsumptionofSoftDrinksDay = @ConsumptionofSoftDrinksDay,
		ConsumptionofCoffeeTeaDay = @ConsumptionofCoffeeTeaDay,
		HaveRegularMeals = @HaveRegularMeals,
		EatHomeFoodOutsidePredominantly = @EatHomeFoodOutsidePredominantly
	WHERE 
		IDLifestyle = @IDLifestyle
END
GO

CREATE PROCEDURE deleteLifestyle
	@IDLifestyle INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Lifestyle
	WHERE 
		IDLifestyle = @IDLifestyle

END
GO

Create Table BasicComplaints(
IDBasicComplaints int primary key identity,
 StatementofComplaint nvarchar(max),
 HistoryofPreviousTreatment nvarchar(max),
  PhysicianHospitalTreated nvarchar(max),
)
go

CREATE PROCEDURE CreateBasicComplaints

	 @StatementofComplaint nvarchar(max),
 @HistoryofPreviousTreatment nvarchar(max),
  @PhysicianHospitalTreated nvarchar(max),
	@IDBasicComplaints INT OUTPUT
AS 
BEGIN 
	INSERT INTO BasicComplaints VALUES(@StatementofComplaint, @HistoryofPreviousTreatment, @PhysicianHospitalTreated)
	SET @IDBasicComplaints = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getBasicComplaints
	@IDBasicComplaints INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		BasicComplaints
	WHERE 
		IDBasicComplaints = @IDBasicComplaints
END
GO

CREATE PROCEDURE updateBasicComplaints
	 @StatementofComplaint nvarchar(max),
 @HistoryofPreviousTreatment nvarchar(max),
  @PhysicianHospitalTreated nvarchar(max),
	@IDBasicComplaints INT 
	 
AS 
BEGIN 
	UPDATE BasicComplaints SET 
		
		StatementofComplaint = @StatementofComplaint, 
		HistoryofPreviousTreatment = @HistoryofPreviousTreatment,
		PhysicianHospitalTreated = @PhysicianHospitalTreated
	WHERE 
		IDBasicComplaints = @IDBasicComplaints
END
GO


CREATE PROCEDURE deleteBasicComplaints
	@IDBasicComplaints INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			BasicComplaints
	WHERE 
		IDBasicComplaints = @IDBasicComplaints

END
GO

Create Table ImportantMedicalComplaints(
IDImportantMedicalComplaints int primary key identity,
 Diabetic nvarchar(max),
 Hypertensive nvarchar(max),
  CardiacCondition nvarchar(max),
   OrthopedicCondition nvarchar(max),
 MuscularCondition nvarchar(max),
  NeurologicalCondition nvarchar(max),
   DigestiveCondition nvarchar(max),
 KnownAllergies nvarchar(max),
   KnownAdverseReactiontoSpecificDrugs nvarchar(max),
   MajorSurgeries nvarchar(max),
)
go

CREATE PROCEDURE CreateImportantMedicalComplaints
 @Diabetic nvarchar(max),
@Hypertensive nvarchar(max),
@CardiacCondition nvarchar(max),
@OrthopedicCondition nvarchar(max),
 @MuscularCondition nvarchar(max),
@NeurologicalCondition nvarchar(max),
@DigestiveCondition nvarchar(max),
 @KnownAllergies nvarchar(max),
   @KnownAdverseReactiontoSpecificDrugs nvarchar(max),
   @MajorSurgeries nvarchar(max),
	@IDImportantMedicalComplaints INT OUTPUT
AS 
BEGIN 
	INSERT INTO ImportantMedicalComplaints VALUES( @Diabetic,
@Hypertensive ,
@CardiacCondition,
@OrthopedicCondition ,
 @MuscularCondition,
@NeurologicalCondition,
@DigestiveCondition ,
 @KnownAllergies ,
   @KnownAdverseReactiontoSpecificDrugs,
   @MajorSurgeries )
	SET @IDImportantMedicalComplaints = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getImportantMedicalComplaints
	@IDImportantMedicalComplaints INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		ImportantMedicalComplaints
	WHERE 
		IDImportantMedicalComplaints = @IDImportantMedicalComplaints
END
go

CREATE PROCEDURE updateImportantMedicalComplaints

 @Diabetic nvarchar(max),
@Hypertensive nvarchar(max),
@CardiacCondition nvarchar(max),
@OrthopedicCondition nvarchar(max),
 @MuscularCondition nvarchar(max),
@NeurologicalCondition nvarchar(max),
@DigestiveCondition nvarchar(max),
 @KnownAllergies nvarchar(max),
   @KnownAdverseReactiontoSpecificDrugs nvarchar(max),
   @MajorSurgeries nvarchar(max),
	@IDImportantMedicalComplaints INT 
	 
AS 
BEGIN 
	UPDATE ImportantMedicalComplaints SET 
		
		Diabetic = @Diabetic, 
		Hypertensive = @Hypertensive,
		CardiacCondition = @CardiacCondition,
		OrthopedicCondition = @OrthopedicCondition,
		MuscularCondition = @MuscularCondition,
		NeurologicalCondition = @NeurologicalCondition,
		DigestiveCondition = @DigestiveCondition,
		KnownAllergies = @KnownAllergies,
		KnownAdverseReactiontoSpecificDrugs = @KnownAdverseReactiontoSpecificDrugs

	WHERE 
		IDImportantMedicalComplaints = @IDImportantMedicalComplaints
END
GO

CREATE PROCEDURE deleteImportantMedicalComplaints
	@IDImportantMedicalComplaints INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			ImportantMedicalComplaints
	WHERE 
		IDImportantMedicalComplaints = @IDImportantMedicalComplaints

END
GO

Create Table Employee(
EmployeeID int primary key identity,
 DateOfBirth nvarchar(100),
)
go

CREATE PROCEDURE CreateEmployee

	
	@DateOfBirth nvarchar(100), 
 
	@EmployeeID INT OUTPUT
AS 
BEGIN 
	INSERT INTO Employee VALUES(@DateOfBirth)
	SET @EmployeeID = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getEmployee
	@EmployeeID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Employee
	WHERE 
		EmployeeID = @EmployeeID
END
GO

CREATE PROCEDURE updateEmployee

	@DateOfBirth nvarchar(100), 
 
	@EmployeeID INT
	 
AS 
BEGIN 
	UPDATE Employee SET 
		
		DateOfBirth = @DateOfBirth
	WHERE 
		EmployeeID = @EmployeeID
END
GO

CREATE PROCEDURE deleteEmployee
	@EmployeeID INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Employee
	WHERE 
		EmployeeID = @EmployeeID

END
GO

create Table Doctor(
DID Int primary key identity,
 EmployeeID int foreign key references Employee(EmployeeID),
 PersonalDetails int foreign key references PersonalDetails(IDPersonalDetails),
 BasicDetails int foreign key references BasicDetails(IDBasicDetails),  
  ContactDetail int foreign key references ContactDetails(IDContactDetails)
)
go

CREATE PROCEDURE CreateDoctor

    @EmployeeID int, 
	@PersonalDetails int,  
	@BasicDetails int, 
	@ContactDetails int, 
	@DID INT OUTPUT
AS 
BEGIN 
	INSERT INTO Doctor VALUES(@EmployeeID, @PersonalDetails,@BasicDetails,@ContactDetails)
	SET @DID = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getDoctor
	@DID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Doctor
	WHERE 
		DID = @DID
END
GO

GO
create PROCEDURE updateDoctor

	@EmployeeID int, 
	@PersonalDetails int, 
	@BasicDetails int,
	@ContactDetails int,
	@DID INT
	 
AS 
BEGIN 
	UPDATE Doctor SET 
		
		EmployeeID = @EmployeeID,
		PersonalDetails = @PersonalDetails,
		BasicDetails = @BasicDetails,		
		ContactDetail = @ContactDetails
		
		
	WHERE 
		DID = @DID
END
GO

CREATE PROCEDURE deleteDoctor
	@DID INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Doctor
	WHERE 
		DID = @DID

END
GO


Create Table Patient(
OPID int primary key identity,

DID int foreign key references Doctor(DID),
	 BasicDetails int foreign key references BasicDetails(IDBasicDetails),
	  ContactDetails int foreign key references ContactDetails(IDContactDetails),
	  ContactNextOfKin int foreign key references ContactNextOfKin(IDNextOfKin),
	  PersonalDetails int foreign key references PersonalDetails(IDPersonalDetails),
	  Lifestyle int foreign key references Lifestyle(IDLifestyle),
	  BasicComplaints int foreign key references BasicComplaints(IDBasicComplaints),
	  ImportantMedicalComplaints int foreign key references ImportantMedicalComplaints(IDImportantMedicalComplaints),
  

)
go

CREATE PROCEDURE CreatePatient

	 
	@DID int,
	@BasicDetails int, 
	@ContactDetails int, 
	@ContactNextOfKin int, 
	@PersonalDetails int, 
	@Lifestyle int, 
	@BasicComplaints int, 
	@ImportantMedicalComplaints int, 
	@OPID INT OUTPUT
AS 
BEGIN 
	INSERT INTO Patient VALUES(@DID, @BasicDetails, @ContactDetails,@ContactNextOfKin,@PersonalDetails,@Lifestyle,@BasicComplaints,@ImportantMedicalComplaints)
	SET @OPID = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getPatient
	@IDOP INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Patient
	WHERE 
		OPID = @IDOP
END

CREATE PROCEDURE updatePatient

	@DID int,
	@BasicDetails int, 
	@ContactDetails int, 
	@ContactNextOfKin int, 
	@PersonalDetails int, 
	@Lifestyle int, 
	@BasicComplaints int, 
	@ImportantMedicalComplaints int, 
	@OPID INT
	 
AS 
BEGIN 
	UPDATE Patient SET 
		
		DID = @DID,
		BasicDetails = @BasicDetails,
		ContactDetails = @ContactDetails,
		ContactNextOfKin = @ContactNextOfKin,
		PersonalDetails = @PersonalDetails,
		Lifestyle = @Lifestyle,
		BasicComplaints = @BasicComplaints,
		ImportantMedicalComplaints = @ImportantMedicalComplaints
	WHERE 
		OPID = @OPID
END
GO


CREATE PROCEDURE deletePatient
	@OPID INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Patient
	WHERE 
		OPID = @OPID

END
GO

create PROCEDURE getPatientForDoctor
	@DID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Patient
	WHERE 
		DID = @DID
END
GO


Create Table Appointment(
IDAppointment Int primary key identity,
 PatientID int foreign key references Patient(OPID),
 DoctorID int foreign key references Doctor(DID),
 DateAppointment nvarchar(max)
)
go

CREATE PROCEDURE CreateAppointment
    @PatientID int, 
	@DoctorID int, 
	@DateAppointment nvarchar(max),  
	@IDAppointment INT OUTPUT
AS 
BEGIN 
	INSERT INTO Appointment VALUES(@PatientID, @DoctorID,@DateAppointment)
	SET @IDAppointment = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getAppointment
	@IDAppointment INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Appointment
	WHERE 
		IDAppointment = @IDAppointment
END
GO

CREATE PROCEDURE getAppointmentForPatient
	@PatientID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Appointment
	WHERE 
		PatientID = @PatientID
END
GO


CREATE PROCEDURE getAppointmentForDoctor
	@DoctorID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Appointment
	WHERE 
		DoctorID = @DoctorID
END
GO

GO
CREATE PROCEDURE updateAppointment

    @PatientID int, 
	@DoctorID int, 
	@DateAppointment nvarchar(max),  
	@IDAppointment INT
	 
AS 
BEGIN 
	UPDATE Appointment SET 
	
		PatientID = @PatientID,
		DoctorID = @DoctorID,
		DateAppointment = @DateAppointment
		
		
	WHERE 
		IDAppointment = @IDAppointment
END
GO


CREATE PROCEDURE deleteAppointment
	@IDAppointment INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Appointment
	WHERE 
		IDAppointment = @IDAppointment
END
GO


Create Table Bill(
IDBill Int primary key identity,
 PatientID int foreign key references Patient(OPID),
 BillItem nvarchar(max),
 BillPrice int
)
go

CREATE PROCEDURE CreateBill
    @PatientID int, 
	@BillItem nvarchar(max), 
	@BillPrice int, 
	@IDBill INT OUTPUT
AS 
BEGIN 
	INSERT INTO Bill VALUES(@PatientID, @BillItem, @BillPrice)
	SET @IDBill = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getBill
	@IDBill INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Bill
	WHERE 
		IDBill = @IDBill
END
GO

CREATE PROCEDURE getBillForPatient
	@PatientID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		Bill
	WHERE 
		PatientID = @PatientID
END
GO

GO
CREATE PROCEDURE updateBill

    @PatientID int, 
	@BillItem nvarchar(max), 
	@BillPrice int, 
	@IDBill INT
	 
AS 
BEGIN 
	UPDATE Bill SET 
	
		PatientID = @PatientID,
		BillItem = @BillItem,
		BillPrice = @BillPrice		
	WHERE 
		IDBill = @IDBill
END
GO


CREATE PROCEDURE deleteBill
	@IDBill INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			Bill
	WHERE 
		IDBill = @IDBill

END
GO












/*Drop Table RegistrationForm*/


select * from Patient
select * from Doctor
select * from BasicDetails
select * from PersonalDetails
select * from ContactDetails
select * from ContactNextOfKin
select * from Lifestyle
select * from BasicComplaints
select * from ImportantMedicalComplaints

select * from RegistrationForm
select * from MiniRegistrationForm

create Table  RegistrationForm(
 IDForm int Primary key identity,
    IDOP int foreign key references Patient(OPID),
	 BasicDetails int foreign key references BasicDetails(IDBasicDetails),
	  ContactDetails int foreign key references ContactDetails(IDContactDetails),
	  ContactNextOfKin int foreign key references ContactNextOfKin(IDNextOfKin),
	  PersonalDetails int foreign key references PersonalDetails(IDPersonalDetails),
	  Lifestyle int foreign key references Lifestyle(IDLifestyle),
	  BasicComplaints int foreign key references BasicComplaints(IDBasicComplaints),
	  ImportantMedicalComplaints int foreign key references ImportantMedicalComplaints(IDImportantMedicalComplaints),
)
go

create PROCEDURE CreateRegistrationForm

	@IDOP int, 
	@BasicDetails int, 
	@ContactDetails int, 
	@ContactNextOfKin int, 
	@PersonalDetails int, 
	@Lifestyle int, 
	@BasicComplaints int, 
	@ImportantMedicalComplaints int, 
	@IDForm INT OUTPUT
AS 
BEGIN 
	INSERT INTO RegistrationForm VALUES(@IDOP, @BasicDetails, @ContactDetails,@ContactNextOfKin,@PersonalDetails,@Lifestyle,@BasicComplaints,@ImportantMedicalComplaints)
	SET @IDForm = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getRegistrationForm
	@IDForm INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		RegistrationForm
	WHERE 
		IDForm = @IDForm
END
GO

CREATE PROCEDURE getRegistrationForms
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		RegistrationForm
END
GO

CREATE PROCEDURE getRegistrationFormForPatient
	@OPID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		RegistrationForm
	WHERE 
		IDOP = @OPID
END
GO

CREATE PROCEDURE updateRegistrationForm


	@IDOP int, 
	@BasicDetails int, 
	@ContactDetails int, 
	@ContactNextOfKin int, 
	@PersonalDetails int, 
	@Lifestyle int, 
	@BasicComplaints int, 
	@ImportantMedicalComplaints int, 
	@IDForm INT
	 
AS 
BEGIN 
	UPDATE RegistrationForm SET 
		
		IDOP = @IDOP,
		BasicDetails = @BasicDetails,
		ContactDetails = @ContactDetails,
		ContactNextOfKin = @ContactNextOfKin,
		PersonalDetails = @PersonalDetails,
		Lifestyle = @Lifestyle,
		BasicComplaints = @BasicComplaints,
		ImportantMedicalComplaints = @ImportantMedicalComplaints
	WHERE 
		IDForm = @IDForm
END
GO


CREATE PROCEDURE deleteRegistrationForm
	@IDForm INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			RegistrationForm
	WHERE 
		IDForm = @IDForm

END
GO

/*drop table MiniRegistrationForm*/


Create Table MiniRegistrationForm(
 IDForm int Primary key identity,
 PatientName nvarchar(100),
 Sex nvarchar(1),
 DateOfBirth nvarchar(max),
 BriefStatementOfComplain nvarchar(max),
 ContactTelNumber1 nvarchar(20),
 ContactTelNumber2 nvarchar(20),
 NextOfKinName nvarchar(80),
 NOKRelationshipToPatitent nvarchar(10),
   IDOP int foreign key references Patient(OPID)
)
go
CREATE PROCEDURE CreateMiniRegistrationForm
	@PatientName NVARCHAR(20),
	@Sex NVARCHAR(1),
	@DateOfBirth nvarchar(max),
	@BriefStatementOfComplain nvarchar(max), 
	@ContactTelNumber1 nvarchar(20), 
	@ContactTelNumber2 nvarchar(20), 
	@NextOfKinName nvarchar(80),
	@NOKRelationshipToPatitent nvarchar(10), 
	@IDOP int, 
	@IDForm INT OUTPUT
AS 
BEGIN 

	INSERT INTO MiniRegistrationForm VALUES(@PatientName, @Sex, @DateOfBirth,@BriefStatementOfComplain,@ContactTelNumber2,@ContactTelNumber1,@NextOfKinName,@NOKRelationshipToPatitent,@IDOP)
	SET @IDForm = SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE getMiniRegistrationForm
	@IDForm INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		MiniRegistrationForm
	WHERE 
		IDForm = @IDForm
END
GO

CREATE PROCEDURE getMiniRegistrationForms
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		MiniRegistrationForm
END
GO

CREATE PROCEDURE getMiniRegistrationFormForPatient
	@OPID INT
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		MiniRegistrationForm
	WHERE 
		IDOP = @OPID
END
GO
CREATE PROCEDURE updateMiniRegistrationForm


		@PatientName NVARCHAR(20),
	@Sex NVARCHAR(1),
	@DateOfBirth nvarchar(max),
	@BriefStatementOfComplain nvarchar(max), 
	@ContactTelNumber2 nvarchar(20), 
	@ContactTelNumber1 nvarchar(20), 
		@NextOfKinName nvarchar(80),
	@NOKRelationshipToPatitent nvarchar(10), 
	@IDOP int, 
	@IDForm INT
	 
AS 
BEGIN 
	UPDATE MiniRegistrationForm SET 
		
		PatientName = @PatientName,
		Sex = @Sex,
		DateOfBirth = @DateOfBirth,
		BriefStatementOfComplain = @BriefStatementOfComplain,
		ContactTelNumber2 = @ContactTelNumber2,
		ContactTelNumber1 = @ContactTelNumber1,
		NextOfKinName=@NextOfKinName,
		NOKRelationshipToPatitent = @NOKRelationshipToPatitent,
		IDOP = @IDOP
	WHERE 
		IDForm = @IDForm
END
GO


CREATE PROCEDURE deleteMiniRegistrationForm
	@IDForm INT	 
AS 
BEGIN 
	DELETE  
	FROM 
			MiniRegistrationForm
	WHERE 
		IDForm = @IDForm

END
GO