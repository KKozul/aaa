﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class ObrokJelovnik
    {
        public int IDObrokJelovnik { get; set; }
        public Jelovnik ObrokJelovnik_Jelovnik { get; set; }
        public Obrok NazivObrokJelovnik { get; set; }
        public Namirnica MastNamirnicaJelovnik { get; set; }
        public Namirnica UgljikohidratNamirnicaJelovnik { get; set; }
        public Namirnica BjelancevinaNamirnicaJelovnik { get; set; }
    }
}
