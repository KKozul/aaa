﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Model;
using Microsoft.ApplicationBlocks.Data;

namespace DAL.Repo
{
    class SqlRepository : IRepo
    {
        private string dbCS = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public void DeleteJelovnik(int IDJelovnik)
        {
            throw new NotImplementedException();
        }

        public void DeleteKombinacija(int IDKombinacija)
        {
            throw new NotImplementedException();
        }

        public void DeleteKorisnik(int IDKorisnik)
        {
            throw new NotImplementedException();
        }

        public void DeleteMjernaJedinica(int IDMjernaJedinica)
        {
            throw new NotImplementedException();
        }

        public void DeleteNamirnica(int IDNamirnica)
        {
            SqlHelper.ExecuteNonQuery(dbCS, "deleteNamirnica",
                           new SqlParameter("@IDNamirnica", IDNamirnica));
        }

        public void DeleteObrok(int IDObrok)
        {
            SqlHelper.ExecuteNonQuery(dbCS, "deleteObrok",
                           new SqlParameter("@IDObrok", IDObrok));
        }

        public void DeleteObrokJelovnik(int IDObrokJelovnik)
        {
            throw new NotImplementedException();
        }

        public List<Jelovnik> GetJelovnici()
        {
            throw new NotImplementedException();
        }

        public Jelovnik GetJelovnik(int IDJelovnik)
        {
            throw new NotImplementedException();
        }

        public Kombinacija GetKombinacija(int IDKombinacija)
        {
            throw new NotImplementedException();
        }

        public List<Kombinacija> GetKombinacije()
        {
            throw new NotImplementedException();
        }

        public List<Korisnik> GetKorisnici()
        {
            throw new NotImplementedException();
        }

        public Korisnik GetKorisnik(int IDKorisnik)
        {
            throw new NotImplementedException();
        }

        public MjernaJedinica GetMjernaJedinica(int IDMjernaJedinica)
        {
            throw new NotImplementedException();
        }

        public List<MjernaJedinica> GetMjerneJedinice()
        {
            throw new NotImplementedException();
        }

        public Namirnica GetNamirnica(int IDNamirnica)
        {
            Namirnica namirnica = null;
            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getNamirnica", new SqlParameter("@IDNamirnica", IDNamirnica));
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    namirnica = new Namirnica(sdr.GetInt32(0), sdr.GetString(1), sdr.GetInt32(2), sdr.GetInt32(3), (TipoviNamirnica)sdr.GetInt32(4), sdr.GetBoolean(5));
                }
            }
            sdr.Close();
            return namirnica;
        }

        public List<Namirnica> GetNamirnice()
        {
            List<Namirnica> namirnice = new List<Namirnica>();
            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getNamirnice");
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    namirnice.Add(new Namirnica(sdr.GetInt32(0), sdr.GetString(1), sdr.GetInt32(2), sdr.GetInt32(3), (TipoviNamirnica)sdr.GetInt32(4), sdr.GetBoolean(5)));
                }
            }
            sdr.Close();
            return namirnice;
        }

        public List<Obrok> GetObroci()
        {
            List<Obrok> obroci = new List<Obrok>();
            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getObroci");
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    obroci.Add(new Obrok(sdr.GetInt32(0), sdr.GetString(1), sdr.GetBoolean(2)));
                }
            }
            sdr.Close();
            return obroci;
        }

        public Obrok GetObrok(int IDObrok)
        {
            Obrok obrok = null;
            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getObrok", new SqlParameter("@IDObrok", IDObrok));
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    obrok = new Obrok(sdr.GetInt32(0), sdr.GetString(1), sdr.GetBoolean(2));
                }
            }
            sdr.Close();
            return obrok;
        }

        public List<ObrokJelovnik> GetObrokiJelovnik()
        {
            throw new NotImplementedException();
        }

        public List<ObrokJelovnik> GetObrokiJelovnikFromJelovnik(int IDJelovnik)
        {
            throw new NotImplementedException();
        }

        public ObrokJelovnik GetObrokJelovnik(int IDObrokJelovnik)
        {
            throw new NotImplementedException();
        }

        public int InsertJelovnik(Jelovnik jelovnik)
        {
            throw new NotImplementedException();
        }

        public int InsertKombinacija(Kombinacija kombinacija)
        {
            throw new NotImplementedException();
        }

        public int InsertKorisnik(Korisnik korisnik)
        {
            throw new NotImplementedException();
        }

        public int InsertMjernaJedinica(MjernaJedinica mjernaJedinica)
        {
            throw new NotImplementedException();
        }

        public int InsertNamirnica(Namirnica namirnica)
        {

           int retVal = SqlHelper.ExecuteNonQuery(dbCS, "createNamirnica",
                           new SqlParameter("@NazivNamirnica", namirnica.NazivNamirnica), new SqlParameter("@EnergijaKJNamirnica", namirnica.EnergijaKJNamirnica),
                           new SqlParameter("@EnergijaKcalNamirnica", namirnica.EnergijaKcalNamirnica), new SqlParameter("@TipNamirnica", (int)namirnica.TipNamirnica),
                            new SqlParameter("@OmogucenaNamirnica", namirnica.OmogucenaNamirnica), new SqlParameter("@IDNamirnica", SqlDbType.Int));

            return retVal;
        }

        public int InsertObrok(Obrok obrok)
        {
            int retVal = SqlHelper.ExecuteNonQuery(dbCS, "createObrok",
                            new SqlParameter("@NazivObrok", obrok.NazivObrok), new SqlParameter("@OmogucenObrok", obrok.OmogucenObrok), new SqlParameter("@IDObrok", SqlDbType.Int));

            return retVal;
        }

        public int InsertObrokJelovnik(ObrokJelovnik obrok)
        {
            throw new NotImplementedException();
        }

        public void UpdateJelovnik(Jelovnik jelovnik, int IDJelovnik)
        {
            throw new NotImplementedException();
        }

        public void UpdateKombinacija(Kombinacija kombinacija, int IDKombinacija)
        {
            throw new NotImplementedException();
        }

        public void UpdateKorisnik(Korisnik korisnik, int IDKorisnik)
        {
            throw new NotImplementedException();
        }

        public void UpdateMjernaJedinica(MjernaJedinica mjernaJedinica, int IDMjernaJedinica)
        {
            throw new NotImplementedException();
        }

        public void UpdateNamirnica(Namirnica namirnica, int IDNamirnica)
        {
            SqlHelper.ExecuteNonQuery(dbCS, "updateNamirnica",
                           new SqlParameter("@NazivNamirnica", namirnica.NazivNamirnica), new SqlParameter("@EnergijaKJNamirnica", namirnica.EnergijaKJNamirnica),
                           new SqlParameter("@EnergijaKcalNamirnica", namirnica.EnergijaKcalNamirnica), new SqlParameter("@TipNamirnica", (int)namirnica.TipNamirnica),
                            new SqlParameter("@OmogucenaNamirnica", namirnica.OmogucenaNamirnica), new SqlParameter("@IDNamirnica", IDNamirnica));
        }

        public void UpdateObrok(Obrok obrok, int IDObrok)
        {
            SqlHelper.ExecuteNonQuery(dbCS, "updateObrok",
                           new SqlParameter("@NazivObrok", obrok.NazivObrok), new SqlParameter("@OmogucenObrok", obrok.OmogucenObrok), new SqlParameter("@IDObrok", IDObrok));
        }

        public void UpdateObrokJelovnik(ObrokJelovnik obrok, int IDObrokJelovnik)
        {
            throw new NotImplementedException();
        }
    }
}
