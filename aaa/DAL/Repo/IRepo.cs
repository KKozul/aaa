﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repo
{
    public interface IRepo
    {
        int InsertKorisnik(Korisnik korisnik);
        void UpdateKorisnik(Korisnik korisnik, int IDKorisnik);
        Korisnik GetKorisnik(int IDKorisnik);
        List<Korisnik> GetKorisnici();
        void DeleteKorisnik(int IDKorisnik);

        int InsertNamirnica(Namirnica namirnica);
        void UpdateNamirnica(Namirnica namirnica, int IDNamirnica);
        Namirnica GetNamirnica(int IDNamirnica);
        List<Namirnica> GetNamirnice();
        void DeleteNamirnica(int IDNamirnica);

        int InsertJelovnik(Jelovnik jelovnik);
        void UpdateJelovnik(Jelovnik jelovnik, int IDJelovnik);
        Jelovnik GetJelovnik(int IDJelovnik);
        List<Jelovnik> GetJelovnici();
        void DeleteJelovnik(int IDJelovnik);
        
        int InsertKombinacija(Kombinacija kombinacija);
        void UpdateKombinacija(Kombinacija kombinacija, int IDKombinacija);
        Kombinacija GetKombinacija(int IDKombinacija);
        List<Kombinacija> GetKombinacije();
        void DeleteKombinacija(int IDKombinacija);

        int InsertMjernaJedinica(MjernaJedinica mjernaJedinica);
        void UpdateMjernaJedinica(MjernaJedinica mjernaJedinica, int IDMjernaJedinica);
        MjernaJedinica GetMjernaJedinica(int IDMjernaJedinica);
        List<MjernaJedinica> GetMjerneJedinice();
        void DeleteMjernaJedinica(int IDMjernaJedinica);

        int InsertObrok(Obrok obrok);
        void UpdateObrok(Obrok obrok, int IDObrok);
        Obrok GetObrok(int IDObrok);
        List<Obrok> GetObroci();
        void DeleteObrok(int IDObrok);

        int InsertObrokJelovnik(ObrokJelovnik obrok);
        void UpdateObrokJelovnik(ObrokJelovnik obrok, int IDObrokJelovnik);
        ObrokJelovnik GetObrokJelovnik(int IDObrokJelovnik);
        List<ObrokJelovnik> GetObrokiJelovnik();
        List<ObrokJelovnik> GetObrokiJelovnikFromJelovnik(int IDJelovnik);

        void DeleteObrokJelovnik(int IDObrokJelovnik);



    }
}
