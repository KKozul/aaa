﻿using DAL.Model;
using DAL.Repo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class PrikazObroka : System.Web.UI.Page
    {
        private List<Obrok> obroci = new List<Obrok>();
        private IRepo repository;
        protected void Page_Load(object sender, EventArgs e)
        {
            repository = RepoFactory.GetRepo();

            obroci = repository.GetObroci();

            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                    repNamirnice.DataSource = obroci;
                    repNamirnice.DataBind();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DodajObrok.aspx");
        }

        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
            int obrokID = int.Parse((item.FindControl("lblIDObrok") as Label).Text);
            Response.Redirect("~/UrediObrok.aspx?id=" + obrokID);
        }

        protected void btnDisable_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
            int obrokID = int.Parse((item.FindControl("lblIDObrok") as Label).Text);
            Obrok tempObrok = null;
            foreach (Obrok obrok in obroci)
            {
                if (obrok.IDObrok == obrokID)
                {
                    tempObrok = obrok;
                }
            }
            if (tempObrok != null)
            {
                tempObrok.OmogucenObrok = false;
                repository.UpdateObrok(tempObrok, obrokID);
            }
            Response.Redirect("~/PrikazObroka.aspx");
        }

        protected void btnEnable_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
            int obrokID = int.Parse((item.FindControl("lblIDObrok") as Label).Text);
            Obrok tempObrok = null;
            foreach (Obrok obrok in obroci)
            {
                if (obrok.IDObrok == obrokID)
                {
                    tempObrok = obrok;
                }
            }
            if (tempObrok != null)
            {
                tempObrok.OmogucenObrok = true;
                repository.UpdateObrok(tempObrok, obrokID);
            }
            Response.Redirect("~/PrikazObroka.aspx");
        }
    }
}