﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DodajNamirnicu.aspx.cs" Inherits="WebFormApp.DodajNamirnicu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <title></title>
</head> 
<body>
<form id="form1" runat="server">            
<div class="container">
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            <asp:ImageButton ID="imgLogo" runat="server" Height="100%" Width="100%"  ImageUrl="~/logo.png" OnClick="imgLogo_Click"/>
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1" style="margin-top:5px">
            <asp:Label ID="lblUsername" Text="Username" runat="server" CssClass="label label-info" />
        </div>       
        <div class="col-md-1">
            <asp:Button ID="btnMenu" Text="Nazad" runat="server" CssClass="btn btn-danger" OnClick="btnMenu_Click" />
        </div>
    </div>    
    <h1>DODAVANJE NAMIRNICE</h1>
    
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            Naziv namirnice:
        </div>        
        <div class="col-md-2">
             <asp:TextBox ID="txtBoxNamirnicaNaziv" runat="server" CssClass="form-control" />
        </div>  
    </div>
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            Tip namirnice:
        </div>        
        <div class="col-md-2">
            <asp:RadioButtonList ID="rbListTipNamirnice" runat="server" >
                <asp:ListItem Text="Mast" />
                <asp:ListItem Text="Ugljikohidrat" />
                <asp:ListItem Text="Bjelančevina" />
            </asp:RadioButtonList>
        </div>  
    </div>
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            Energija namirnice u KJ :
        </div>        
        <div class="col-md-2">
             <asp:TextBox ID="txtBoxNamirnicaKJ" runat="server" CssClass="form-control" />
        </div>  
    </div>
    
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            Energija namirnice u Kcal:
        </div>        
        <div class="col-md-2 form-group">
             <asp:TextBox ID="txtBoxNamirnicaKcal" runat="server" CssClass="form-control" />
        </div>  
    </div>

    <div class="row">
        <asp:Button ID="btnAdd" Text="Dodaj Namirnicu" runat="server" CssClass="btn btn-info" OnClick="btnAdd_Click" />
    </div>
</div>
</form>

</body>
</html>
