﻿using DAL.Model;
using DAL.Repo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class DodajObrok : System.Web.UI.Page
    {
        private IRepo repository;

        protected void Page_Load(object sender, EventArgs e)
        {
            repository = RepoFactory.GetRepo();
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }


        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazObroka.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string nazivObroka = txtBoxNazivObrok.Text;

                repository.InsertObrok(new Obrok(0, nazivObroka, true));
                Response.Redirect("~/PrikazObroka.aspx");
            }
            catch (Exception exception)
            {
                Response.Redirect("~/PrikazObroka.aspx");
            }


        }
    }
}