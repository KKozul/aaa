﻿using DAL.Model;
using DAL.Repo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class UrediObrok : System.Web.UI.Page
    {
        private Obrok obrok;
        private IRepo repository;

        protected void Page_Load(object sender, EventArgs e)
        {
            repository = RepoFactory.GetRepo();

            obrok = null;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                try
                {
                    obrok = repository.GetObrok(int.Parse(Request.QueryString["id"]));
                }
                catch (Exception exception)
                {
                    Response.Redirect("~/PrikazObroka.aspx");
                }

            }

            if (obrok == null)
                Response.Redirect("~/PrikazObroka.aspx");



            if (!IsPostBack)
            {
                txtBoxNazivObrok.Text = obrok.NazivObrok;
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazObroka.aspx");
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                string nazivObroka = txtBoxNazivObrok.Text;

                repository.UpdateObrok(new Obrok(obrok.IDObrok, nazivObroka, true), obrok.IDObrok);
                Response.Redirect("~/PrikazObroka.aspx");
            }
            catch (Exception exception)
            {
                Response.Redirect("~/UrediObrok.aspx?id=" + obrok.IDObrok);
            }


        }
    }
}