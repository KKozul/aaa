﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrikazNamirnice.aspx.cs" Inherits="WebFormApp.PrikazNamirnice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />

</head>
<body>
<form id="form1" runat="server">            
<div class="container">
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            <asp:ImageButton ID="imgLogo" runat="server" Height="100%" Width="100%"  ImageUrl="~/logo.png" OnClick="imgLogo_Click"/>
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1" style="margin-top:5px">
            <asp:Label ID="lblUsername" Text="Username" runat="server" CssClass="label label-info" />
        </div>       
        <div class="col-md-1">
            <asp:Button ID="btnMenu" Text="Na Izbornik" runat="server" CssClass="btn btn-danger" OnClick="btnMenu_Click" />
        </div>
    </div>
    <h1>NAMIRNICE</h1>
        <div class="row" style="margin-top:10px">
        <div class="col-md-1">
            <asp:Label Text="Filteri:" runat="server" />
        </div>
        
        <div class="col-md-4">            
            Tip Namirnice:
            <asp:CheckBox ID="checkBoxMasti" Text="Masti" runat="server" />
            <asp:CheckBox ID="checkBoxUgljikohidrati" Text="Ugljikohidrati" runat="server" />
            <asp:CheckBox ID="checkBoxBjelancevine" Text="Bjelančevine" runat="server" />
        </div>                

        <div class="col-md-2">            
            <asp:Button ID="btnApplyFilter" Text="Primjeni Filtere" runat="server" CssClass="btn btn-info" OnClick="btnApplyFilter_Click" />
        </div>
        
    </div>
    <div class="row" style="margin-top:100px">
        
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Naziv</td>
                    <td>Tip Namirnice</td>
                    <td>Postavke</td>

                </tr>
            </thead>
        <asp:Repeater ID="repNamirnice" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:Label ID="lblIDNamirnica" runat="server" Text='<%# Eval("IDNamirnica") %>' /></td>
                    <td><asp:Label ID="lblNazivNamirnica" runat="server" Text='<%# Eval("NazivNamirnica") %>' /></td>
                    <td><asp:Label ID="lblTipNamirnica" runat="server" Text='<%# Eval("TipNamirnica").ToString() %>' /></td>
                    <td>
                        <asp:Button ID="btnEdit" Text="Uredi" runat="server" CssClass="btn btn-info"  OnClick="btnEdit_Click"/>
                        <asp:Button ID="btnDisable" Text="Onemogući" runat="server" CssClass="btn btn-warning" Visible='<%# (Boolean.Parse(Eval("OmogucenaNamirnica").ToString())? Boolean.Parse("true"): Boolean.Parse("false"))%>' OnClick="btnDisable_Click" />
                        <asp:Button ID="btnEnable" Text="Omogući" runat="server" CssClass="btn btn-success" Visible='<%# (Boolean.Parse(Eval("OmogucenaNamirnica").ToString())? Boolean.Parse("false"): Boolean.Parse("true"))%>' OnClick="btnEnable_Click" />
                    </td>
                </tr>  
            </ItemTemplate>
        </asp:Repeater>            
    </table>
    </div>
    <div class="row">
        <asp:Button ID="btnAddItem" Text="Dodaj Namirnicu" runat="server" CssClass="btn btn-info" OnClick="btnAddItem_Click" />
    </div>
</div>
</form>
</body>
</html>
