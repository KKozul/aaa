﻿using DAL.Model;
using DAL.Repo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class DodajNamirnicu : System.Web.UI.Page
    {
        private IRepo repository;

        protected void Page_Load(object sender, EventArgs e)
        {
            repository = RepoFactory.GetRepo();
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }


        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazNamirnice.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string nazivNamirnice = txtBoxNamirnicaNaziv.Text;
                TipoviNamirnica tipNamirnice = (TipoviNamirnica)rbListTipNamirnice.SelectedIndex;

                int energijaKJNamirnice = int.Parse(txtBoxNamirnicaKJ.Text);
                int energijaKcalNamirnice = int.Parse(txtBoxNamirnicaKcal.Text);

                repository.InsertNamirnica(new Namirnica(0, nazivNamirnice, energijaKJNamirnice, energijaKcalNamirnice, tipNamirnice,true));
                Response.Redirect("~/PrikazNamirnice.aspx");
            }
            catch (Exception exception)
            {
                Response.Redirect("~/PrikazNamirnice.aspx");
            }


        }
    }
}