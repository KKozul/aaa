﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebFormApp.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
   <title></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: small">
   <form id="form1" runat="server">    
    <div class="container">
     <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            <asp:ImageButton ID="imgLogo" runat="server" Height="100%" Width="100%"  ImageUrl="~/logo.png" OnClick="imgLogo_Click"/>
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1" style="margin-top:5px">
            <asp:Label ID="lblEatBetter" Text="Eat better Admin Site" runat="server" CssClass="label label-default" />
        </div>       
        <div class="col-md-1">
            
    </div>
    </div>  
      <div style="padding-top:px">
         <asp:PlaceHolder runat="server" ID="LoginStatus" Visible="false">
            <p>
               <asp:Literal runat="server" ID="StatusText" />
            </p>
         </asp:PlaceHolder>
         <asp:PlaceHolder runat="server" ID="LoginForm" Visible="false">
            <div style="margin-bottom: 10px;margin-left:45%; margin-right:45%">
               <asp:Label runat="server" AssociatedControlID="UserName" CssClass="label label-primary">User name</asp:Label>
               <div class="form-group">
                  <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
               </div>
            </div>
            <div style="margin-bottom: 10px;margin-left:45%;margin-right:45%">
               <asp:Label runat="server" AssociatedControlID="Password" CssClass="label label-primary">Password</asp:Label>
               <div class="form-group">
                  <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
               </div>
            </div>
            <div style="margin-bottom: 10px;margin-left:48.5%">
               <div>
                  <asp:Button CssClass="btn btn-info" runat="server" OnClick="SignIn" Text="Log in" />
               </div>
            </div>
         </asp:PlaceHolder>
      </div>
        </div>
   </form>
</body>
</html>