﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Izbornik.aspx.cs" Inherits="WebFormApp.Izbornik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
<div class="container">
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            <asp:ImageButton ID="imgLogo" runat="server" Height="100%" Width="100%"  ImageUrl="~/logo.png" OnClick="imgLogo_Click"/>
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1" style="margin-top:5px">
            <asp:Label ID="lblUsername" Text="Username" runat="server" CssClass="label label-info" />
        </div>        
        <div class="col-md-1">
            <asp:Button ID="btnLogOut" Text="Odjava" runat="server" CssClass="btn btn-danger" OnClick="btnLogOut_Click" />
        </div>
    </div>
    
    <div class="row" style="margin-top:100px">
        <div class="col-md-offset-3 col-md-1"><asp:Label ID="Label1" Text="KORISNICI" runat="server" CssClass="label label-default"/></div>
        <div class="col-md-1"><asp:Label ID="Label2" Text="NAMIRNICE" runat="server" CssClass="label label-default"/></div>
        <div class="col-md-1"><asp:Label ID="Label3" Text="OBROCI" runat="server" CssClass="label label-default"/></div>
        <div class="col-md-1" style="margin-left:-20px"><asp:Label ID="Label4" Text="MJERNE JEDINICE" runat="server" CssClass="label label-default"/></div>
        <div class="col-md-1" style="margin-left:20px"><asp:Label ID="Label5" Text="KOMBINACIJE" runat="server" CssClass="label label-default"/></div>
    </div>
    <div class="row">
        <div class="col-md-offset-3 col-md-1">
            <asp:Button ID="btnKorisnici" Text="Prikaz" runat="server" CssClass="btn btn-primary" OnClick="btnKorisnici_Click" />
        </div>
        <div class="col-md-1">
            <asp:Button ID="btnNamirnice" Text="Prikaz" runat="server"  CssClass="btn btn-primary" OnClick="btnNamirnice_Click"/>
        </div>
        <div class="col-md-1">
            <asp:Button ID="btnObroci" Text="Prikaz" runat="server"  CssClass="btn btn-primary" OnClick="btnObroci_Click"/>
        </div>
        <div class="col-md-1">
            <asp:Button Text="Prikaz" runat="server"  CssClass="btn btn-primary" />
        </div>
        <div class="col-md-1">
            <asp:Button Text="Prikaz" runat="server"  CssClass="btn btn-primary" />
        </div>
    </div>
</div>
    </form>
</body>
</html>
