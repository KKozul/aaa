﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class Izbornik : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/Login.aspx");
        }

        protected void btnKorisnici_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazKorisnici.aspx");
        }

        protected void btnNamirnice_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazNamirnice.aspx");
        }

        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnObroci_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazObroka.aspx");
        }
    }
}