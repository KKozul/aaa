﻿using DAL.Model;
using DAL.Repo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class UrediNamirnicu : System.Web.UI.Page
    {
        private Namirnica namirnica;
        private IRepo repository;

        protected void Page_Load(object sender, EventArgs e)
        {
            repository = RepoFactory.GetRepo();

            namirnica = null;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                try
                {
                    namirnica = repository.GetNamirnica(int.Parse(Request.QueryString["id"]));
                }
                catch (Exception exception)
                {
                    Response.Redirect("~/PrikazNamirnice.aspx");
                }
               
            }

            if (namirnica == null)
                Response.Redirect("~/PrikazNamirnice.aspx");



            if (!IsPostBack)
            {
                txtBoxNamirnicaNaziv.Text = namirnica.NazivNamirnica;
                txtBoxNamirnicaKJ.Text = namirnica.EnergijaKJNamirnica.ToString();
                txtBoxNamirnicaKcal.Text = namirnica.EnergijaKcalNamirnica.ToString();

                rbListTipNamirnice.SelectedIndex = (int)namirnica.TipNamirnica;
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PrikazNamirnice.aspx");
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                string nazivNamirnice = txtBoxNamirnicaNaziv.Text;
                TipoviNamirnica tipNamirnice = (TipoviNamirnica)rbListTipNamirnice.SelectedIndex;

                int energijaKJNamirnice = int.Parse(txtBoxNamirnicaKJ.Text);
                int energijaKcalNamirnice = int.Parse(txtBoxNamirnicaKcal.Text);

                repository.UpdateNamirnica(new Namirnica(namirnica.IDNamirnica, nazivNamirnice, energijaKJNamirnice, energijaKcalNamirnice, tipNamirnice, namirnica.OmogucenaNamirnica), namirnica.IDNamirnica);
                Response.Redirect("~/PrikazNamirnice.aspx");
            }
            catch (Exception exception)
            {
                Response.Redirect("~/UrediNamirnicu.aspx?id=" + namirnica.IDNamirnica);
            }


        }

    }
}