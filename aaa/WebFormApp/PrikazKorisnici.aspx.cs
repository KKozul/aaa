﻿using DAL.Model;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class PrikazKorisnici : System.Web.UI.Page
    {
        private string dbCS = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        private List<Korisnik> korisnici = new List<Korisnik>();
        public List<bool> filteri = new List<bool> { false, false, false, false, false, false, false};

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["filters"]))
            {
                ProcessFilters(Request.QueryString["filters"]);
            }
            
            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getKorisnici");
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    Korisnik tempKorisnik = new Korisnik(sdr.GetInt32(0), sdr.GetString(1), sdr.GetString(2), sdr.GetDateTime(3), sdr.GetString(4), sdr.GetString(5), sdr.GetInt32(6), sdr.GetInt32(7), sdr.GetInt32(8), sdr.GetInt32(9), sdr.GetString(10));
                    bool canAddKorisnik = false;

                    if (filteri[0] == false && filteri[1] == false && filteri[2] == false && filteri[3] == false && filteri[4] == false && filteri[5] == false && filteri[6] == false)
                            canAddKorisnik = true;

                    if((tempKorisnik.SpolKorisnik == "M" && filteri[0] == true) || (tempKorisnik.SpolKorisnik == "Z" && filteri[1] == true) 
                        || (tempKorisnik.RazinaFizickeAktivnostiKorisnik == 1 && filteri[2] == true) || (tempKorisnik.RazinaFizickeAktivnostiKorisnik == 1 && filteri[2] == true)
                        || (tempKorisnik.RazinaFizickeAktivnostiKorisnik == 2 && filteri[3] == true)
                        || (tempKorisnik.RazinaFizickeAktivnostiKorisnik == 3 && filteri[4] == true)
                        || (tempKorisnik.TipDijabetesaKorisnik == 1 && filteri[5] == true)
                        || (tempKorisnik.TipDijabetesaKorisnik == 2 && filteri[6] == true))
                    {
                        canAddKorisnik = true;
                    }

                    if (canAddKorisnik)
                    {
                        korisnici.Add(tempKorisnik);
                    }                    
                }
            }
            sdr.Close();
            

            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                    repKorisnici.DataSource = korisnici;
                    repKorisnici.DataBind();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            else
            {

                
            }


        }

        private void ProcessFilters(string filterString)
        {
            string[] filterStrings = filterString.Split(',');
            for (int i = 0; i < filteri.Count; i++)
            {
                try
                {
                    if (bool.Parse(filterStrings[i]))
                    {
                        filteri[i] = true;
                    }
                    else
                    {
                        filteri[i] = false;
                    }
                }
                catch (Exception e)
                {
                    return;
                }

            }
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            MemoryStream ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms);

            foreach (Korisnik korisnik in korisnici)
            {
                tw.WriteLine($"{korisnik.IDKorisnik},{korisnik.ImeKorisnik} {korisnik.PrezimeKorisnik},{korisnik.TezinaKorisnik},{korisnik.VisinaKorisnik},{korisnik.SpolKorisnik},{korisnik.RazinaFizickeAktivnostiKorisnik},{korisnik.TipDijabetesaKorisnik},{korisnik.EmailKorisnik}");
            }
            tw.Flush();
            byte[] bytes = ms.ToArray();
            ms.Close();

            Response.Clear();
            Response.ContentType = "application/force-download";
            Response.AddHeader("content-disposition", "attachment;    filename=korisnici.csv");
            Response.BinaryWrite(bytes);
            Response.End();
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            filteri[0] = checkBoxMale.Checked;
            filteri[1] = checkBoxFemale.Checked;
            filteri[2] = checkBoxPhys1.Checked;
            filteri[3] = checkBoxPhys2.Checked;
            filteri[4] = checkBoxPhys3.Checked;
            filteri[5] = checkBoxDiabetes1.Checked;
            filteri[6] = checkBoxDiabetes2.Checked;

            Response.Redirect("~/PrikazKorisnici.aspx?filters=" + $"{filteri[0]},{filteri[1]},{filteri[2]},{filteri[3]},{filteri[4]},{filteri[5]},{filteri[6]}");
        }

        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }
    }
}