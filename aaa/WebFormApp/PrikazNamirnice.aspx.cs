﻿using DAL.Model;
using DAL.Repo;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class PrikazNamirnice : System.Web.UI.Page
    {
        private List<Namirnica> namirnice = new List<Namirnica>();
        public List<bool> filteri = new List<bool> { false, false, false };
        private IRepo repository;
        protected void Page_Load(object sender, EventArgs e)
        {
            repository = RepoFactory.GetRepo();

            List<Namirnica> tempNamirnice = repository.GetNamirnice();

            if (!string.IsNullOrEmpty(Request.QueryString["filters"]))
            {
                ProcessFilters(Request.QueryString["filters"]);
            }


            foreach (var namirnica in tempNamirnice)
            {
                bool canAddNamirnica = false;
                if (filteri[0] == false && filteri[1] == false && filteri[2] == false)
                    canAddNamirnica = true;

                if ((namirnica.TipNamirnica == TipoviNamirnica.Mast && filteri[0] == true) || (namirnica.TipNamirnica == TipoviNamirnica.Ugljikohidrat && filteri[1] == true) || (namirnica.TipNamirnica == TipoviNamirnica.Bjelančevina && filteri[2] == true))
                {
                    canAddNamirnica = true;
                }

                if (canAddNamirnica)
                {
                    namirnice.Add(namirnica);
                }
            }


            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                    repNamirnice.DataSource = namirnice;
                    repNamirnice.DataBind();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DodajNamirnicu.aspx");
        }

        protected void btnDisable_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
            int namirnicaID = int.Parse((item.FindControl("lblIDNamirnica") as Label).Text);
            Namirnica tempNamirnica = null;
            foreach (Namirnica namirnica in namirnice)
            {
                if(namirnica.IDNamirnica == namirnicaID)
                {
                    tempNamirnica = namirnica;
                }
            }
            if(tempNamirnica != null)
            {
                tempNamirnica.OmogucenaNamirnica = false;
                repository.UpdateNamirnica(tempNamirnica, namirnicaID);
            }
            Response.Redirect("~/PrikazNamirnice.aspx");
        }

        protected void btnEnable_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
            int namirnicaID = int.Parse((item.FindControl("lblIDNamirnica") as Label).Text);
            Namirnica tempNamirnica = null;
            foreach (Namirnica namirnica in namirnice)
            {
                if (namirnica.IDNamirnica == namirnicaID)
                {
                    tempNamirnica = namirnica;
                }
            }
            if (tempNamirnica != null)
            {
                tempNamirnica.OmogucenaNamirnica = true;
                repository.UpdateNamirnica(tempNamirnica, namirnicaID);
            }
            Response.Redirect("~/PrikazNamirnice.aspx");
        }

        protected void imgLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            filteri[0] = checkBoxMasti.Checked;
            filteri[1] = checkBoxUgljikohidrati.Checked;
            filteri[2] = checkBoxBjelancevine.Checked;

            Response.Redirect("~/PrikazNamirnice.aspx?filters=" + $"{filteri[0]},{filteri[1]},{filteri[2]}");
        }

        private void ProcessFilters(string filterString)
        {
            string[] filterStrings = filterString.Split(',');
            for (int i = 0; i < filteri.Count; i++)
            {
                try
                {
                    if (bool.Parse(filterStrings[i]))
                    {
                        filteri[i] = true;
                    }
                    else
                    {
                        filteri[i] = false;
                    }
                }
                catch (Exception e)
                {
                    return;
                }

            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
            int namirnicaID = int.Parse((item.FindControl("lblIDNamirnica") as Label).Text);
            Response.Redirect("~/UrediNamirnicu.aspx?id=" + namirnicaID);
        }
    }
}