﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrikazKorisnici.aspx.cs" Inherits="WebFormApp.PrikazKorisnici" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />

</head>
<body>
    
<form id="form1" runat="server">            
<div class="container">
    <div class="row" style="margin-top:10px">
        <div class="col-md-2">
            <asp:ImageButton ID="imgLogo" runat="server" Height="100%" Width="100%"  ImageUrl="~/logo.png" OnClick="imgLogo_Click"/>
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1" style="margin-top:5px">
            <asp:Label ID="lblUsername" Text="Username" runat="server" CssClass="label label-info" />
        </div>       
        <div class="col-md-1">
            <asp:Button ID="btnMenu" Text="Na Izbornik" runat="server" CssClass="btn btn-danger" OnClick="btnMenu_Click" />
        </div>
    </div>    
    <h1>KORISNICI</h1>
    <div class="row" style="margin-top:10px">
        <div class="col-md-1">
            <asp:Label Text="Filteri:" runat="server" />
        </div>
        
        <div class="col-md-2">            
            Spol:
            <asp:CheckBox ID="checkBoxMale" Text="M" runat="server" />
            <asp:CheckBox ID="checkBoxFemale" Text="Ž" runat="server" />
        </div>        
        
        <div class="col-md-3">            
            Razina Fizičke Aktivnosti:
            <asp:CheckBox ID="checkBoxPhys1" Text="1" runat="server" />
            <asp:CheckBox ID="checkBoxPhys2" Text="2" runat="server" />
            <asp:CheckBox ID="checkBoxPhys3" Text="3" runat="server" />
        </div>   
        
        <div class="col-md-2">            
            Tip Dijabetesa:
            <asp:CheckBox ID="checkBoxDiabetes1" Text="1" runat="server" />
            <asp:CheckBox ID="checkBoxDiabetes2" Text="2" runat="server" />
        </div>        

        <div class="col-md-2">            
            <asp:Button ID="btnApplyFilter" Text="Primjeni Filtere" runat="server" CssClass="btn btn-info" OnClick="btnApplyFilter_Click" />
        </div>
        
    </div>

    <div class="row table-responsive" style="margin-top:40px">

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Ime i prezime:</td>
                    <td>Datum rođenja</td>
                    <td>Težina</td>
                    <td>Visina</td>
                    <td>Spol</td>
                    <td>Razina Fizičke Aktivnosti</td>
                    <td>Tip dijabetesa</td>
                    <td>E-mail</td>
                </tr>
            </thead>
        <asp:Repeater ID="repKorisnici" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("IDKorisnik") %>' /></td>
                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("ImeKorisnik") + " " + Eval("PrezimeKorisnik") %>' /></td>
                    <td><asp:Label ID="Label3" runat="server" Text='<%# String.Format("{0:d}", Eval("DOBKorisnik")) %>' /></td>
                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("TezinaKorisnik") %>' /></td>
                    <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("VisinaKorisnik") %>' /></td>
                    <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("SpolKorisnik") %>' /></td>
                    <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("RazinaFizickeAktivnostiKorisnik") %>' /></td>
                    <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("TipDijabetesaKorisnik") %>' /></td>
                    <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("EmailKorisnik") %>' /></td>
                </tr>  
            </ItemTemplate>
        </asp:Repeater>            
    </table>
    </div>
    <div class="row">
        <asp:Button ID="btnExport" Text="Izvoz" runat="server" CssClass="btn btn-info" OnClick="btnExport_Click" />
    </div>
</div>
</form>

</body>
</html>
