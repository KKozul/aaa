﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    class Obrok
    {
        public int IDObrok { get; set; }
        public string NazivObrok { get; set; }
    }
}
