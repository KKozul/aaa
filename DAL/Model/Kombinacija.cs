﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    class Kombinacija
    {
        public int IDKombinacija { get; set; }
        public int BrojObrokaKombinacija { get; set; }
        public DateTime DatumPocetkaKombinacija { get; set; }
        public DateTime DatumZavrsetkaKombinacija { get; set; }
    }
}
