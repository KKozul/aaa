﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public enum TipoviNamirnica
    {
        Mast, Ugljikohidrat, Bjelančevina
    }
    public class Namirnica
    {
        public int IDNamirnica { get; set; }
        public string NazivNamirnica { get; set; }
        public int EnergijaKJNamirnica { get; set; }
        public int EnergijaKcalNamirnica { get; set; }
        public TipoviNamirnica TipNamirnica { get; set; }

        public Namirnica(int IDNamirnica, string NazivNamirnica, int EnergijaKJNamirnica, int EnergijaKcalNamirnica, TipoviNamirnica TipNamirnica)
        {
            this.IDNamirnica = IDNamirnica;
            this.NazivNamirnica = NazivNamirnica;
            this.EnergijaKJNamirnica = EnergijaKJNamirnica;
            this.EnergijaKcalNamirnica = EnergijaKcalNamirnica;
            this.TipNamirnica = TipNamirnica;
        }
    }
}
