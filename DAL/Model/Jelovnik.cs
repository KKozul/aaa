﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    class Jelovnik
    {
        public int IDJelovnik { get; set; }
        public int BrojObrokaJelovnik { get; set; }
        public DateTime DatumJelovnik { get; set; }
        public List<ObrokJelovnik> MyProperty { get; set; }
    }
}
