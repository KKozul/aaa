﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    class MjernaJedinica
    {
        public int IDMjernaJedinica { get; set; }
        public string NazivMjernaJedinica { get; set; }
    }
}
