﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Izbornik.aspx.cs" Inherits="WebFormApp.Izbornik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <asp:Image ID="imgLogo" runat="server" ImageUrl="https://www.childhood.org.au/app/uploads/2017/07/ACF-logo-placeholder.png" Height="35%" Width="35%" />
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1">
            <asp:Label ID="lblUsername" Text="Username" runat="server" />
        </div>        
        <div class="col-md-1">
            <asp:Button ID="btnLogOut" Text="Odjava" runat="server" OnClick="btnLogOut_Click" />
        </div>
    </div>

    <div class="row" style="margin-top:100px">
        <div class="col-md-offset-3 col-md-1">KORISNICI</div>
        <div class="col-md-1">NAMIRNICE</div>
        <div class="col-md-1">OBROCI</div>
        <div class="col-md-1">MJERNE JEDINICE</div>
        <div class="col-md-1">KOMBINACIJE</div>
    </div>
    <div class="row">
        <div class="col-md-offset-3 col-md-1">
            <asp:Button ID="btnKorisnici" Text="Prikaz" runat="server" OnClick="btnKorisnici_Click" />
        </div>
        <div class="col-md-1">
            <asp:Button ID="btnNamirnice" Text="Prikaz" runat="server" OnClick="btnNamirnice_Click"/>
        </div>
        <div class="col-md-1">
            <asp:Button Text="Prikaz" runat="server" />
        </div>
        <div class="col-md-1">
            <asp:Button Text="Prikaz" runat="server" />
        </div>
        <div class="col-md-1">
            <asp:Button Text="Prikaz" runat="server" />
        </div>
    </div>
</div>
    </form>
</body>
</html>
