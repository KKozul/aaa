﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrikazKorisnici.aspx.cs" Inherits="WebFormApp.PrikazKorisnici" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</head>
<body>
    
<form id="form1" runat="server">            
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <asp:Image ID="imgLogo" runat="server" ImageUrl="https://www.childhood.org.au/app/uploads/2017/07/ACF-logo-placeholder.png" Height="35%" Width="35%" />
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1">
            <asp:Label ID="lblUsername" Text="Username" runat="server" />
        </div>        
        <div class="col-md-1">
            <asp:Button ID="btnMenu" Text="Na Izbornik" runat="server" OnClick="btnMenu_Click" />
        </div>
    </div>

    <div class="row" style="margin-top:100px">
        <h1>KORISNICI</h1>
        <table class="table table-bordered">
            <thead class="">
                <tr>
                    <td>#</td>
                    <td>Ime i prezime:</td>
                    <td>Datum rođenja</td>
                    <td>Težina</td>
                    <td>Visina</td>
                    <td>Spol</td>
                    <td>Razina Fizičke Aktivnosti</td>
                    <td>Tip dijabetesa</td>
                    <td>E-mail</td>
                </tr>
            </thead>
        <asp:Repeater ID="repKorisnici" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("IDKorisnik") %>' /></td>
                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("ImeKorisnik") + " " + Eval("PrezimeKorisnik") %>' /></td>
                    <td><asp:Label ID="Label3" runat="server" Text='<%# String.Format("{0:d}", Eval("DOBKorisnik")) %>' /></td>
                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("TezinaKorisnik") %>' /></td>
                    <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("VisinaKorisnik") %>' /></td>
                    <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("SpolKorisnik") %>' /></td>
                    <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("RazinaFizickeAktivnostiKorisnik") %>' /></td>
                    <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("TipDijabetesaKorisnik") %>' /></td>
                    <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("EmailKorisnik") %>' /></td>
                </tr>  
            </ItemTemplate>
        </asp:Repeater>            
    </table>
    </div>
    <div class="row">
        <asp:Button ID="btnExport" Text="Izvoz" runat="server" OnClick="btnExport_Click" />
    </div>
</div>
</form>

</body>
</html>
