﻿using DAL.Model;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class PrikazKorisnici : System.Web.UI.Page
    {
        private string dbCS = ConfigurationManager.ConnectionStrings["WebAppDBConnection"].ConnectionString;
        private List<Korisnik> korisnici = new List<Korisnik>();
        protected void Page_Load(object sender, EventArgs e)
        {

            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getKorisnici");
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    korisnici.Add(new Korisnik(sdr.GetInt32(0), sdr.GetString(1), sdr.GetString(2), sdr.GetDateTime(3), sdr.GetString(4), sdr.GetString(5), sdr.GetInt32(6), sdr.GetInt32(7), sdr.GetInt32(8), sdr.GetInt32(9), sdr.GetString(10)));
                }
            }
            sdr.Close();
            

            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                    repKorisnici.DataSource = korisnici;
                    repKorisnici.DataBind();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            MemoryStream ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms);

            foreach (Korisnik korisnik in korisnici)
            {
                tw.WriteLine($"{korisnik.IDKorisnik},{korisnik.ImeKorisnik} {korisnik.PrezimeKorisnik},{korisnik.TezinaKorisnik},{korisnik.VisinaKorisnik},{korisnik.SpolKorisnik},{korisnik.RazinaFizickeAktivnostiKorisnik},{korisnik.TipDijabetesaKorisnik},{korisnik.EmailKorisnik}");
            }
            tw.Flush();
            byte[] bytes = ms.ToArray();
            ms.Close();

            Response.Clear();
            Response.ContentType = "application/force-download";
            Response.AddHeader("content-disposition", "attachment;    filename=korisnici.csv");
            Response.BinaryWrite(bytes);
            Response.End();
        }
    }
}