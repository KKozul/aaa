﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrikazNamirnice.aspx.cs" Inherits="WebFormApp.PrikazNamirnice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</head>
<body>
<form id="form1" runat="server">            
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <asp:Image ID="imgLogo" runat="server" ImageUrl="https://www.childhood.org.au/app/uploads/2017/07/ACF-logo-placeholder.png" Height="35%" Width="35%" />
        </div>
        <div class="col-md-offset-2 col-md-6">
            
        </div>
        <div class="col-md-1">
            <asp:Label ID="lblUsername" Text="Username" runat="server" />
        </div>        
        <div class="col-md-1">
            <asp:Button ID="btnMenu" Text="Na Izbornik" runat="server" OnClick="btnMenu_Click" />
        </div>
    </div>

    <div class="row" style="margin-top:100px">
        <h1>NAMIRNICE</h1>
        <table class="table table-bordered">
            <thead class="">
                <tr>
                    <td>#</td>
                    <td>Naziv</td>
                    <td>Tip Namirnice</td>
                    <td>Postavke</td>

                </tr>
            </thead>
        <asp:Repeater ID="repNamirnice" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("IDNamirnica") %>' /></td>
                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("NazivNamirnica") %>' /></td>
                    <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("TipNamirnica").ToString() %>' /></td>
                    <td>
                        <asp:Button ID="btnEdit" Text="Uredi" runat="server" />
                        <asp:Button ID="btnDisable" Text="Onemogući" runat="server" />
                    </td>
                </tr>  
            </ItemTemplate>
        </asp:Repeater>            
    </table>
    </div>
    <div class="row">
        <asp:Button ID="btnAddItem" Text="Dodaj Namirnicu" runat="server" OnClick="btnAddItem_Click" />
    </div>
</div>
</form>
</body>
</html>
