﻿using DAL.Model;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormApp
{
    public partial class PrikazNamirnice : System.Web.UI.Page
    {
        private string dbCS = ConfigurationManager.ConnectionStrings["WebAppDBConnection"].ConnectionString;
        private List<Namirnica> namirnice = new List<Namirnica>();
        protected void Page_Load(object sender, EventArgs e)
        {

            SqlDataReader sdr = SqlHelper.ExecuteReader(dbCS, System.Data.CommandType.StoredProcedure, "getNamirnice");
            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    namirnice.Add(new Namirnica(sdr.GetInt32(0), sdr.GetString(1), sdr.GetInt32(2), sdr.GetInt32(3), (TipoviNamirnica)sdr.GetInt32(4)));
                }
            }
            sdr.Close();


            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblUsername.Text = User.Identity.GetUserName();
                    repNamirnice.DataSource = namirnice;
                    repNamirnice.DataBind();
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Izbornik.aspx");
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {

        }
    }
}